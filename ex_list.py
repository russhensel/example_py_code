# -*- coding: utf-8 -*-

"""
Notes:

    Some things that can be done with lists, but not all
    You should generally assume that any simple thing that can be done with
    lists can be done with Python lists.

    Also many of the thing that can be done with lists can also be done with
    string, tuples, sets, dicts.....


    See also:
        ex_list_comphrension.py    to quickly build new list without explicit looping - fast
        ex_list_slice.py           to pull out subsets of a list
        ex_looping.py              to loop through a list

    Status of this file: functional but not finished and with old junk


Search for:

        append           add an item to end of list
        access           access by index
        add              combine 2 or more lists to new list
        boolean          tbd
        copy             tbd
        comprehension    not here see ex_list_comphrension.py
        defines          simple ways to define a list
        equality         tbd
        find             not a function to find an item, use index
        in               true if an item is in a list
        insert           insert an item into a list
        index            get the index of an item if it is in a list
        length           how big is the list
        len              for getting the length
        looping          see: ex_looping.py
        modify           using an index or by insert
        prepend          tbd
        pop              tbd -- kind of delete need to add
        range            make an itterater, easy to change it into a list
        reverse          reverse a list
        search           -- use find above
        slice            see: ex_list_slice.py
        sort   sorted    to sort lists


"""
import types
from collections import OrderedDict


# ----------------------------------------
# note that lists are mutable strings are not
def ex_defines():
    print( """\n\n
    ======== ex_defines() simple defines  ========
    """ )
    print( "define a list of names each item is a string " )
    a_list_of_names      = [ "John", "Sue", "Mary", "Jane" ]
    print( ) # blank line
    print( a_list_of_names )

    print( "define a list of numbers each item is a some kind of number " )

    a_list_of_numbers    = [1, 22, 37, 93, 49, 3.1415926, -3]
    print( )
    print( a_list_of_numbers )

    print( "define a list of mixed types " )
    a_list                = [1, 22, "twenty seven", 93, 49, 3.1415926, -3, "john"]
    print( )
    print( a_list )

    print("define a list of numbers using the range function and list ")
    a_list                = list(range( 3, 17))   # changed in python 3: range now  acts like old xrange
    print( )
    print(a_list)

    print("most itterables can be made into a list with the list function here a sting ")
    a_list                = list("Mr. President")   # changed in python 3: range now  acts like old xrange
    print( )
    print(a_list)

#ex_defines()

# -------------------------------
def ex_length():
    print("""\n\n
    ======== ex_length(): len() length of list  ========

    """)
    a_list    =  [ "a", "b", "c", "d"     ]
    list_length =  len( a_list )             # not a_list.len()

    print( f"the list {a_list} has length {list_length}" )

#ex_length()

# -------------------------------
def ex_access_by_index():
    print("""\n\n
    ======== ex_access_by_index():use square brackets  ========
    indicies should be integers >= 0 and < len( of the list ) else
    you will throw an exception
    """)
    a_list    =  [ "a", "b", "c", "d"     ]

    a_index  = 3
    print( f"the {a_index} th (based from 0) element of {a_list} is {a_list[a_index]}" )

    # will throw an error if not a valid index ... can use try except
    a_index    = 7    # try other indicies  try floats ... they do not work
    try:
        print( f"the {a_index} th is {a_list[a_index]}" )
    except:
        print( f"there is no a_list[{a_index}]" )

    print( 'you can also modify by index, try a_list[2] = "C"'  )
    a_list[2] = "C"                    # need to have valid index
    print( f"a_list is now {a_list}")

    a_list    =  [ "a", "b", "c", "d"     ]
    print( 'you can also insert by index mutates list'  )
    a_list.insert(0, "new_val_at_0")
    print( a_list )

    a_list    =  [ "a", "b", "c", "d"     ]
    print( 'you can also insert by index -- mutates list'  )
    a_list.insert(2, "new_val_at_2")
    print( a_list )


ex_access_by_index()

# -------------------------------
def ex_2d_lists():
    print("""\n\n
    ======== ex_2d_lists(): put a list in a list  ========
    """)
    a_list    =  [ [1,2,3], [5,7,7],  [1,2,3], ]

    print( f"the  2, 2 in a_list is {a_list[2][2]}" )   # copy line an try other indicies

#ex_2d_lists()


# ----------------------------------------
def ex_append_extend_add():
    print( """\n\n
    ======== ex_append_extend() append add an item, extend add a list, addition  ========
    """ )

    # append, often useful inside a loop, list comphrension often an alternative
    # append may start from an empty list []
    a_list    = [1, 2, 3, ]   # try changing to an empty list
    a_list.append( 4 )
    a_list.append( 5 )
    a_list.append( 6 )
    print( f"a_list now mutated via append to: {a_list}")

    a_list    = [1, 2, 3, ]
    b_list    = [4, 5, 6, ]
    a_list.extend( b_list )
    print( f"a_list now mutated via append to: {a_list}")

    a_list    = [1, 2, 3, ]
    b_list    = [4, 5, 6, ]
    c_list    = a_list + b_list
    print( f"a_list + b_list makes a new list: {c_list}")

#ex_append_extend_add()

#----------------------------
def ex_sort_sorted_simple():
    print("""\n\n
    ======== ex_sort_sorted(): simple example ========
    """)
    a_list = [0,5,2,3,4,12,2,7,8]
    #print a[2:5]
    print()
    print(f"\noriginal list {a_list}")

    b_list = sorted( a_list )      # produces a new list a_list is unchanged
    print()
    print(f"\nnew list, ( orginal list not changed )   {b_list}")


    a_list.sort(   )          # sort in place -- susposed to be faster no extra list
    print()
    print(f"\noriginal list now changed {a_list}")


#ex_sort_sorted_simple()


# -------------------------------
def ex_find():
    print("""\n\n
    ======== ex_find() find in a list with index() and in ========
    https://stackoverflow.com/questions/9542738/python-find-in-list
    """)
    a_list    =  [ "a", "b", "c", "d"     ]

    a_item    = "c"
    a_index   =  a_list.index( a_item )
    print( f"{a_item} is the {a_index} th item ( base 0 ) in the list {a_list}" )

    # will throw an error if not found in list so need to use try except
    try:
        print((a_list.index( "xx" )))
    except:
        print( "index function threw exception " )


    # use "in" instead of index just gives true false

    is_in_list    = "a" in a_list
    print( f"a in the list a_list {is_in_list}" )

    # using in you can be sure not to index out of range
    # but this is slower than try except

    a_item        = "a"     # try with other items
    if a_item in a_list:
        print( f"a in the list a_list {is_in_list}" )
        at_index   = a_list.index( a_item )   # will never be out of range
        print( f"{a_item} found at {at_index}")
    else:
        print( f"{a_item} found not in {a_list}")

# ex_find()


# -------------------------------
def ex_reverse():
    print("""
    ================ ex_reverse() reverse a list===============
    """)
    a_list   = [ 1, 2, 3, 4, ]

    print( f"the list is: {a_list}" )

    a_list.reverse();
    print( f"a_list.reverse() mutates the list to: {a_list}" )


# ex_reverse()







#================ see old file ==================

#------------------ define create  ------------
# note that lists are mutable strings are not
def ex_copy():
    print( """
    ================ ex_copy() not a deep copy - more than 1 way... equality ===============

    """ )

    #
    a_list_of_names      = [ "John", "Sue", "Mary", "Jane" ]
    b_list               = list( a_list_of_names )
    print( b_list )

    b_list               = list( list( a_list_of_names ) )

    print( b_list )
    print( f"are the two lists equal {a_list_of_names == b_list }")
    print( f"are the two lists same  {a_list_of_names is b_list }")

    # slice
    b_list    = a_list_of_names[ : ]
    print( b_list )
    print( f"are the two lists equal {a_list_of_names == b_list }")
    print( f"are the two lists same  {a_list_of_names is b_list }")

#------------
    list_a   = get_a_list( "a_dict" )       # "a_str" "a_tuple" "a_dict" "odict" "set" "x"
    list_b   = get_a_list( "a_dict" )

    print( list_a )


    print( f"are the two lists equal {list_a == list_b }")
    print( f"are the two lists same  {list_a is list_b }")

    # ---- as boolean
    a_list   = []
    if a_list:
        print( "empty list True ish" )
    else:
        print( "empty list False ish" )

#ex_copy()

# ====================================================================
def ex_modify_1xxx():
    print("""
    ================ ex_modify_1() modify items insert prepend ===============

    """)

    print("---------- insert but may be slow -------------")
    a = ["v1", "v2", "v3", "v4"]
    print( a)
    a.insert(0, "new val  1at 0")
    print( a )
    a = ["new value2 at 0 "] + a
    print( a )
    a.insert( 0, "prepended item" )   # this is best there is with a list, if used alot perhaps another collection
    print( a )


#------------------- modify still need push pop for queues ----------------------
# ====================================================================
def ex_modify_1xxx():
    print("""
    ================ ex_modify_1() modify items insert prepend ===============

    """)

    print("---------- insert but may be slow -------------")
    a = ["v1", "v2", "v3", "v4"]
    print( a)
    a.insert(0, "new val  1at 0")
    print( a )
    a = ["new value2 at 0 "] + a
    print( a )
    a.insert( 0, "prepended item" )   # this is best there is with a list, if used alot perhaps another collection
    print( a )


#ex_modify_1()

# ====================================================================
def ex_append_remove_addxxxx():
    print("""
    ================ ex_append_remove_add() add append del delete  vs extend timing memory uncler move modify here? ===============

    """)
    alpha_list     = ["a", "b", "c", "d", "e", "f", "g", ]
    aalpha_list    = ["aa", "bb", "cc", "dd", "ee", "ff", "gg", ]

    print( "----------add-----------")
    print( "add of two lists creates a new list original list unchange ")
    print( f"alpha_list = {alpha_list}" )
    print( f"aalpha_list = {aalpha_list}" )

    print( f"alpha_list + aalpha_list = {alpha_list + aalpha_list}" )

    print( f"alpha_list = {alpha_list}" )
    print( f"aalpha_list = {aalpha_list}" )

    print( "----------extend-----------")
    print( f"alpha_list.extend( aalpha_list ) = {alpha_list.extend( aalpha_list )}" )
    print( f"alpha_list = {alpha_list}" )
    print( f"aalpha_list = {aalpha_list}" )

    print( "slice, usually, at least creates a new list, not a mutate ")

    print( "append remove... modify, mutate a list ")
    a_list = []
    print(a_list)

    a_list.append( 1 )
    a_list.append( 2)
    a_list.append( "joe")
    a_list.append( 5)
    a_list.append( 99)
    print(a_list)
    a_list.remove( "joe" )   # delete by value ... what if two 2 of them
    print(a_list)
    del a_list[2:4]
    print(a_list)
    del a_list[0]    # delete an element
    print(a_list)


# ex_append_remove_add()

#------------------ access  ------------
# ====================================================================
def ex_access():
    print("""
    ================ ex_access() access items ===============
    You can get at an item in a list by using an index into the lists,
    the index is just the numerical position of the item in the list with
    the first item being called 0 ( not in some ways the more logical 1 ).
    see also ex_list_slice.py
    """)

    a_list_of_names      = [ "John", "Sue", "Mary", "Jane" ]

    print((a_list_of_names[3]))
    print("""
    String are a lot like lists so you can get at the parts of strings the same way
    """)
    a_string = "a string is a lot like a list of letters"

    print("a_string[0]")
    print((a_string[0]))

    print((a_string[2]))

#ex_access()



#------------------ other ------------
#=============================    xxx  =======================================
def ex_join():
    print("""
    ================ ex_join() simple define and join - makes list into string ===============
    https://69.64.102.59/index.php/Python_Lists
    """)

    # with names of type string:
    a_list_of_names      = [ "John", "Sue", "Mary", "Jane" ]
    print(a_list_of_names)

    print((" ".join( a_list_of_names )))

    # with numbers:
    a_list_of_numbers    = [1, 22, 37, 93, 49, 3.1415926, -3]
    print(a_list_of_numbers)

#ex_join()

#---------------------------- extract access ----------------------



# ====================================================================
def ex_10():
    print("""
    ================ ex_10() basic slice ===============
    basic slice does not inclued upper index
    slice creates a new list
    """)

    zero_to_x   = [0,1,2,3,4,5,6,7,8,9,10]
    print(("len() " + str( len( zero_to_x ) )))

    print((zero_to_x[ 0:3 ]))

    zero_to_x   = "0123456780"      # [0,1,2,3,4,5,6,7,8,9,10]

    print("zero_to_x[ 0:3 ]")
    print((zero_to_x[ 0:3 ]))

    print("zero_to_x[ 3:5 ]")
    print((zero_to_x[ 3:5 ]))


    print("zero_to_x[ 3:999 ]")
    print((zero_to_x[ 3:999 ]))

    print("zero_to_x[ 30:50 ]")
    print((zero_to_x[ 30:50 ]))

#ex_10()






# ====================================================================
def ex_slice():
    print("""
    ================ ex_slice() ===============
    SLICE includes first index, excludes last
    """)
    a = [0,1,2,3,4,5,6,7,8]
    print((a[2:5:2]))

#ex_slice()




# ====================================================================
def ex_sort_sorted_1():
    print("""
    ================ ex_sort_sorted_1() more sort ===============
    this will need some help
    """)
    a = ['COM10', 'COM11', 'COM12', 'COM13', 'COM14', 'COM15', 'COM18', 'COM20', 'COM21', 'COM22', 'COM8', 'COM9' ]
    #print a[2:5]
    print(a)

    print("=== sorted with lambda ===")
    #        lambda arguments: expression
    #        Lambda functions can have any number of arguments but only one expression.
    #        The expression is evaluated and returned. Lambda functions can be used wherever function objects are required.


    # here each item from a is passed to the lambda with the name port which is the argument to the expression
    b = sorted( a, key = lambda port: int( port[3:99] ) )

    #b = sorted( a, key = lambda port: port[3:99]  )
    print(b)


    print("=== now sort with lambda ===")
    a.sort( key = lambda port: int( port[3:99] ) )   # lambda gets port number as int --sort in place supposed to be fastest
    print(a)

    print("=== now sort no lambda just default ===")
    a.sort(   reverse = True )   #
    print(a)

#    newlist = sorted( a, key=lambda x: x.count, reverse=True)
#    print( "next sorted" )
#    print( newlist )

#ex_sort_sorted_1()

# -------------helper------------------
# use this as a key to sort on
def get_age( data ):
    print(data)
    return ( -1 * data[2] )   # reverse the sort

# -------------------------------
def ex_sort_2():
    print("""
    ================ ex_sort_2() sorting with labda function for key ===============
    sorting with labda function for key or another key function
    *>shell   https://wiki.python.org/moin/HowTo/Sorting
    http://pythoncentral.io/how-to-sort-a-list-tuple-or-object-with-sorted-in-python/
    https://developers.google.com/edu/python/sorting
    """)
    student_tuples = [
                    ('john', 'A', 15),
                    ('jane', 'B', 12),
                    ('dave', 'B', 10),
            ]
    sorted_results = sorted( student_tuples, key=lambda student: student[2]) # sort on third element of tuple
    print( sorted_results )

    sorted_results = sorted( student_tuples, key=get_age ) # sort on third element of tuple
    print( sorted_results )

#ex_sort_2()





# -------------- helper -----------------
def printbk( some_text ):
     r_text   = some_text[::-1]
     # s_text   = some_text.reverse()
     print((type(r_text)))
     print(r_text)


#     for i_text in some_text[::-1] :
#         print i_text,
#     print ""
#In Python 3, the print statement has been changed into a function. In Python 3, you can instead do:
#
#print('.', end="")
#This also works in Python 2, provided that you've used from __future__ import print_function.
#
#If you are having trouble with buffering, you can flush the output by adding flush=True keyword argument:
#
#print('.', end="", flush=True)
# -------------------------------
def ex_reversexxxx():
    print("""
    ================ ex_reverse() reverse a list===============
    """)
    a_list   = get_a_list( "string" )

    printbk( a_list )

    a_list   = get_a_list( "strings" )   # reverse will fail

    a_list.reverse();
    print( f"List : {a_list}" )



#ex_reverse()






# ====================================================================


#prefix = i_line[0:4]
#                            print "prefix" + prefix
#values    = [(0,0)] * 10

#print values

# distinguish between arrays and lists
# what is a matric, list of lists not array
#  http://www.thegeekstuff.com/2013/08/python-array/
# when use what
# seems tthat all array elements are of the same type.

# there is also a pythob vector wht is it
# not to mention tuples

#“list comprehensions”. In its simplest form, a list comprehension has the following syntax:
#    L = [expression for variable in sequence]


 #   and L[i:j] returns a new list, containing the objects between i and j.
 #   n = len(L)

  #  item = L[index]

 #   seq = L[start:stop]

#    http://effbot.org/zone/python-list.htm


    #  assignment ok

    # append to a list
# empty list perhaps



# ====================================================================
def t_3():
    print("""
    ================ t_4() ===============
    construct a dict
    """)

    adict    =  { 1:"one",  2:"two"}

    print(adict)

#t_3()


# ====================================================================
def t_6():

    print("""
    ================  t_6() ===============
    looping by more than 1 per loop
    no simple way but might use some of this

    ref:
    python loop by 2 - Google Search
    python loop by 2 - Google Search
        *>url  https://www.google.com/search?client=opera&q=python+loop+by+2&sourceid=opera&ie=UTF-8&oe=UTF-8

    python - How do I loop through a list by twos? - Stack Overflow
        *>url  https://stackoverflow.com/questions/2990121/how-do-i-loop-through-a-list-by-twos

    python - What is the most "pythonic" way to iterate over a list in chunks? - Stack Overflow
        *>url  https://stackoverflow.com/questions/434287/what-is-the-most-pythonic-way-to-iterate-over-a-list-in-chunks

    Changing step in Python loop - Stack Overflow
        *>url  https://stackoverflow.com/questions/46179757/changing-step-in-python-loop/46180145



    """)

    a     = [30,40,50,60,70,80,90,100,120]
    a     = list(range( 0, 40))

    for ix, i_a in enumerate(a):
       if ix%3 == 0:
          print((ix, i_a))


#t_6()

# ========= eof =========
