# -*- coding: utf-8 -*-
#>>>>>python example looping
""""
Notes:

    There are many ways of looping, the most common is to loop over a list or
    other list like thing ( an itterable ).  This is looping using the "for" syntax.

    Looping can also be done with do while syntax, but most often this is not the
    "best" syntax

    Status of this file: functional


Search for:

    break           end loop early
    continue        skip over part of the loop
    else            test if loop finished ( or an if..... )
    enumerate       get an index, or counter for the loop
    for             basic loop
    while           loop with condition test at top
    zip             combine lists to loop over more than one at once




Links:

    python loop by 2 - Google Search
    https://www.google.com/search?client=opera&q=python+loop+by+2&sourceid=opera&ie=UTF-8&oe=UTF-8

    python - How do I loop through a list by twos? - Stack Overflow
    https://stackoverflow.com/questions/2990121/how-do-i-loop-through-a-list-by-twos

    python - What is the most "pythonic" way to iterate over a list in chunks? - Stack Overflow
    https://stackoverflow.com/questions/434287/what-is-the-most-pythonic-way-to-iterate-over-a-list-in-chunks

    Changing step in Python loop - Stack Overflow
    https://stackoverflow.com/questions/46179757/changing-step-in-python-loop/46180145



"""


# ----------------------------------------
def   ex_basic_for_loop():
    print( """\n\n
    ======== ex_basic_for_loop(): simplest loop over a list or other itterable ========
    """ )
    a_list    = [1,"one", "uno"]

    for a_item in a_list:
        print( a_item )

#ex_basic_for_loop()


# ----------------------------------------
def ex_loop_with_counter():
    print( """\n\n
    ======== ex_loop_with_counter(): sometimes you need a counter to index something else: enumerate ========
    """ )
    a_list    = [1,"one", "uno"]

    for ix, a_item in enumerate( a_list):
        print( f"the {ix}th item is {a_item}"  )

#ex_loop_with_counter()

# ----------------------------------------
def ex_loop_through_2_lists():
    print( """\n\n
    ======== ex_loop_through_2_lists(): this is done by combining the lists, then unpacking, uses zip  ========
    """ )
    a_list    = [1,"one", "uno"]
    b_list    = ["two", 2, "II", "duo"]

    for  a_item, b_item in zip( a_list, b_list):
        print( f"the a_item is >{a_item}< the b_item is >{b_item}<"  )

#ex_loop_through_2_lists()

# ----------------------------------------
def ex_loop_with_break_else():
    print( """\n\n
    ======== ex_loop_with_break_else(): else here checks to see if loop 'finished'   ========
    """ )
    a_list    = [1,"one", "uno", "two", 2, "II", "duo", ]

    for  a_item in a_list:
        print( f"the a_item is >{a_item}<"  )
        if a_item == "two":        # try two and xxx here
            break                  # break out of loop, skip the rest
    else:
        print( "else => so we finished the loop" )   # see if we did all items for loop

#ex_loop_with_break_else()

# ----------------------------------------
def ex_loop_with_break_continue():
    print( """\n\n
    ======== ex_loop_with_break_continue(): continue, skip part of loop, break exit loop ========
    """ )
    a_list    = [1,"one", "uno", "two", 2, "II", "duo", ]

    for  a_item in a_list:
        print( f"the a_item is >{a_item}<"  )
        if a_item == 2:             # try with 2 and 3 here ( something in list something not)
            print( "\ncontinue, skip rest of loop body, on to next item\n")
            continue
        if a_item == "twoxx":        # try two and xxx here  ( something in list something not)
            print( "break, skip rest of loop items, exit loop")
            break
        print( "    normal end of loop body" )


#ex_loop_with_break_continue()



# ----------------------------------------
def ex_while_loop():
    print( """\n\n
    ======== ex_while_loop(): not normally used with lists, infrequently the best way ========
    for this you may need to set up your own counter
    """ )
    count = 0
    while (count < 9):
       print(( 'The count is:', count ))
       count = count + 1

    print( "Good bye!" )

    # another example with an else
    count = 0
    while count < 5:
       print(( count, " is  less than 5" ))
       count = count + 1
    else:
       print(( count, " is not less than 5" ))  # how different than falling out the bottom

#ex_while_loop()


# ----------------------------------------
def ex_while_true_loop():
    print( """\n\n
    ======== ex_while_true_loop(): not normally used with lists, for cases where escape is via if ========
    for very indefinite cases, sometimes until program ends
    """ )
    import time
    start_at   = time.time()
    end_at     = start_at + ( 10 )
    while True:
       now_left  = end_at - time.time()
       print( f"still running...{now_left}" )
       time.sleep( .1 )
       if now_left <= 0:
           break

    print( "Good bye!" )


#ex_while_true_loop()


# ----------------------------------------
def ex_when_desperate():
    print( """\n\n
    ======== ex_when_desperate(): if you have a really odd case you can use a while( True )
    with your own counters and escape with an if....this is rarely encountered, but may be
    useful especially if you are unfamiliar with other looping methods
    if you mess up your code may try to run forever. ctrl_c may halt it
    the code below shows some of the ideas, but is junk
    """ )

    counter_1  = 0
    counter_2  = 22
    while True:
       print( f"looping {counter_1}, {counter_2}" )
       counter_1   = counter_2 - 3
       counter_2   = counter_2 + 1
       if counter_1 == counter_2//2:
           print( f"Good bye! {counter_1}, {counter_2}" )
           break





ex_when_desperate()

# ======== eof ========



# ======== eof ========








