# -*- coding: utf-8 -*-

"""
Notes:

    good naming is a skill you get over time: this file shows some
    good as well as bad pratices.


    See also:
        .... nothing for now

    Status of this file: draft



Search for:



Links:

"""

"""
There are lots of names that are part of the Python language, if you use then
as names of your own things, then bad stuff usually happen.  These are often refered
to a reserved names
Some of them are:
            if
            then
            else
            while
            except
            class
            return

Other names to avoid are the names of Python functions, modules and classes:
            print
            exit
            sin
            index
            enumerate

So how to avoid them.  One method is just to mess with them a bit.  Say you are
tempted to use index, just put a_ on as a prefix a_index

            a_index

want to store a return value:

            a_return


"""

"""
It is good if the names in the program closely match the names in the problem
you are trying to solve.

This may be problematic:
    there are no subscripts
    there are only english symbols
    you may collide with a reserved word...

Some solutions
    spell out greek letters  ..... theta  = math.pi/2.
    use _xxx instead of subscripts x_1, y_1, s_n


"""









?? for things I may be thinkink about
!! for things I really think i should do, but have not done yet
# comment
comment at beginning of line ... typically code for debugging
comment indented with code.  comments on the code may apply to a line or a block
comment at end of line  -- comment on code for pretty much that line

put example here


"""

"""

for boolean flags  can we have levels   or _level
    _suffix
    prefix_

    _on
    _is_on
    _bool
    _flag



abr:
    _hi      high
    _lo      low
    _max     maximum
    _min
    _fn     file name
    _ffn    full file name all path stuff

    temp    temperature or temparary
    tmp     temporary
    tansient  temporary ??


    _dir    a directory ( on final / )
    _path   probably a diretory, but couls be ffn
    num
    ix_       interger index  ex:  ix_name
    i_          indexed item   ex   for i_name in names:

        a_    prefix to avoid duplicated or reserved names  ex: a_list

    ret       value to be returned
    msg       a mesage     ex:      msg = "high there", print( msg )

see:

    Python Names - OpenCircuits
    *>url   http://www.opencircuits.com/Python_Names#Generic_Variables





"""


"""
Ned Batchelder: Facts and myths about Python names and values
    *>url  https://nedbatchelder.com/text/names.html


PyVideo.org · PyOhio 2011: Names, Objects, and Plummeting From The Cliff
    *>url  https://pyvideo.org/pyohio-2011/pyohio-2011-names-objects-and-plummeting-from.html



"""


# ----------------------------------------
def   ex_del():
    print("""
    ================ ex_del(): ===============
    but is del useful ??
    """)

    a = 2
    print( f"a {a}" )
    del a
    print( f"a {a}" )  # throws error

ex_del()










