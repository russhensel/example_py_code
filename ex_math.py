# -*- coding: utf-8 -*-



"""
Notes:

    This file contains examples of doing math: in this case calculating
    with numbers with python
    A lot of math is "built in" but a lot is in other modules
    in straight Python the math module is needed for a lot of stuff


    See also:
        .... nothing for now

    Status of this file: functional but not finished and with old junk



Search for:


        absolute
        complex       for complex numbers
        integer
        floats
        factorial
        ptn            power of ten notation
        round
        type

Links:

"""


import math            # for many useful functions
import cmath           # for complex math7


# ----------------------------------
def ex_some_math_types():
    print( """\n\n
    ======== ex_some_math_types(): and the type function  ========
    basic math is done with numbers, but Python has more than one kind
    and more than one kind of arithmetic
    """ )

    a_integer   = 7       # no decimal point so an integer not a float
    print( f"a_integer: {a_integer} is of type {type( a_integer )} ")

    a_number   = 7.       # decimal point   a float
    print( f"a_number: {a_number} is of type {type( a_number )} ")

    a_number   = .007e24       # e is for exponent -- still a float
    print( f"a_number: {a_number} is of type {type( a_number )} ")

    a_number   = 7j       # j not i comes from engineering
    print( f"a_number: {a_number} is of type {type( a_number )} ")

    a_number   = 3 + 7j       # j not i comes from engineering
    print( f"a_number: {a_number} is of type {type( a_number )} ")

    a_number   = 720_000.       # can use _ to make large numbers more readable
    print( f"a_number: {a_number} is of type {type( a_number )} ")

    print()
    print( "indicies should usually be integers")
    print( "integer arithmetic can give surprizes if it is not what you\n"
           "intend, so use a decimal point to get floats ")

    print()
    print( "there are other types, vectors, matricies.... but not here")

#ex_some_math_types()


# ----------------------------------
def ex_some_math_operations():
    print( """/n/n
    ======== ex_some_math_operations: fairly standard symbols ========

    """ )
    print( "many places let you use integers where better usage would be floats")
    print( f"7/2 with automagically be converted to a float {7/2}")

    print( "but to be safe use floats where you mean floats")
    print( f"7./2. with automagically be converted to a float {7./2.}")

    print( f"7. + 2. is as you would expect {7. + 2.}")

    print( f"7. - 2. is as you would expect {7. - 2.}")


    print( f"multiplication uses * so 7. * 2. is as you would expect {7. * 2.}")

    print( f"powers use ** so 7. ** 2. is as you would expect {7. ** 2.}")

    print( f"complex numbers work (7. + 3j) * ( 3. + 2.j ) is   {(7. + 3j) * ( 3. + 2.j )}")

    a_float  = 5.67
    print( f"negation: the minus sign usually works as expected, the negative of a_float {-a_float}")
    print( f"negation 2: to get a negative of a variable you can also multiply: {-1 * a_float}")

    print( "\norder of operations is ususlly what you would expect but \n"
           "good pratice is to be explicit with () and do not try to group\n"
           "with {} [] ......")

ex_some_math_operations()


# ----------------------------------
def ex_integer_math():
    print( """/n/n
    ======== ex_some_math_operations: fairly standard symbols ========

    """ )
    print( "many places let you use integers where better usage would be floats")
    print( f"7/2 with automagically be converted to a float {7/2}")

    a = 1
    b = 2
    int_div  = a // b    # 1/2 = 0
    print( f"interger disision {int_div}"    )                    # integer math division

#ex_integer_math()

# ----------------------------------
def ex_some_functions():
    print( """\n\n
    ======== ex_some_functions():  ========

    """ )

    # some constants are referenced symbolically
    print( f"pi = {math.pi} and e = {math.e}")


    theta       = 2.0
    x_var       = 3.
    y_var       = 4.
    a_exp       = .5
    a_negative  = -3.7
    a_integer   = 7

    print( f"math.sin( theta ) with {theta}   = {math.sin( theta )}" )
    print( f"math.cos( theta ) with {theta}   = {math.cos( theta )}" )
    print( f"math.tan( theta ) with {theta}   = {math.tan( theta )}" )

    print( f"abs( a_negative ) with {a_negative}  = {abs( a_negative )}" )


    # log is base e = ln
    print( f"math.log( x_var ) with {x_var}   = {math.log( x_var )}" )
    # print( f"math.ln( x_var )  with {x_var} = {math.ln( x_var )}" )

    print( f"math.sqrt( {x_var})              = {math.sqrt( x_var )}" )
    print( f"math.pow( {x_var},{y_var} )        = {math.pow( x_var, y_var )}" )
    print( f"math.factorial( {a_integer} )        = {math.factorial( a_integer )}" )

    print( f"e to the x math.exp( {x_var} )    = {math.exp( x_var )}" )

    print( f"\nmore stuff in math  {dir( math )}" )



ex_some_functions()

# ---------------------------------------- what
def ex_round():
    print( """\n
    ================ ex_round():   ===============
    """ )

    '''
    round( x [, n]  )
    Parameters
    x − This is a numeric expression.

    n − Represents number of digits from decimal point up to which x is to be rounded. Default is 0.

    Return Value
    This method returns x rounded to n digits from the decimal point.
    not using floor, not sure what rule is
    Example
    The following example shows the usage of round() method.

    Live Demo
    #!/usr/bin/python3
    '''

    print ("round(70.23456) : ", round(70.23456))
    print ("round(56.659,1) : ", round(56.659,1))
    print ("round(80.264, 2) : ", round(80.264, 2))
    print ("round(100.000056, 3) : ", round(100.000056, 3))
    print ("round(-100.000056, 3) : ", round(-100.000056, 3))


#ex_round()

# ---------------------------------------- what
def ex_buckets_for_word_rank():
    print( """\n
    ================ ex_some_math():   ===============
    this is from a theory , just a series
    """ )
    delta_x      = .2
    x            = 0
    count        = 0
    rank         = 0
    while rank   < 400_000.:
        rank    = math.pow( 10, x )
        print( f"{count} >> {rank}" )
        count   += 1
        x       += delta_x



#ex_buckets_for_word_rank()


# ----------------------------------------
def ex_eval_2():
    print( """
    ================ ex_eval_2(): ===============
    """)

    eval_string  = "print( 3/5 )"
    eval( eval_string )
    eval_it( eval_string )

#ex_eval_2()


    # a_cmnt     = "negative index"
    # a_eval     = 'get_a_list( "strings" )[ 5: :-1 ]'
    # the_result = eval( a_eval )
    # print( f"{a_cmnt}\n{a_eval} is {the_result}\n" )



# ========= eof =========




